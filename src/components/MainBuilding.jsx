import React, { useRef } from 'react';
import { useGLTF } from '@react-three/drei';

const MainBuilding = (props) => {
  const { nodes, materials } = useGLTF('../AVS_MainBuilding.glb');
  return (
    <group {...props} dispose={null}>
      <group
        position={[-146.41, 0, 102.562]}
        rotation={[0, 1.12, 0]}
        scale={0.028}
      >
        <mesh
          castShadow
          receiveShadow
          geometry={nodes.Mesh001.geometry}
          material={materials.Am203_60_mat_004}
        />
        <mesh
          castShadow
          receiveShadow
          geometry={nodes.Mesh001_1.geometry}
          material={materials.Am203_60_mat_001}
        />
        <mesh
          castShadow
          receiveShadow
          geometry={nodes.Mesh001_2.geometry}
          material={materials.Am203_60_mat_002}
        />
        <mesh
          castShadow
          receiveShadow
          geometry={nodes.Mesh001_3.geometry}
          material={materials.Am203_60_mat_003}
        />
        <mesh
          castShadow
          receiveShadow
          geometry={nodes.Mesh001_4.geometry}
          material={materials.Am203_60_mat_005}
        />
        <mesh
          castShadow
          receiveShadow
          geometry={nodes.Mesh001_5.geometry}
          material={materials.Am203_60_mat_006}
        />
        <mesh
          castShadow
          receiveShadow
          geometry={nodes.Mesh001_6.geometry}
          material={materials.Am203_60_mat_007}
        />
        <mesh
          castShadow
          receiveShadow
          geometry={nodes.Mesh001_7.geometry}
          material={materials.Am203_60_mat_008}
        />
        <mesh
          castShadow
          receiveShadow
          geometry={nodes.Mesh001_8.geometry}
          material={materials.Am203_60_mat_009}
        />
        <mesh
          castShadow
          receiveShadow
          geometry={nodes.Mesh001_9.geometry}
          material={materials.roofprop_002_01}
        />
        <mesh
          castShadow
          receiveShadow
          geometry={nodes.Mesh001_10.geometry}
          material={materials.roofprop_001_01}
        />
        <mesh
          castShadow
          receiveShadow
          geometry={nodes.Mesh001_11.geometry}
          material={materials.roofprop_004_01}
        />
        <mesh
          castShadow
          receiveShadow
          geometry={nodes.Mesh001_12.geometry}
          material={materials.roofprop_006_01}
        />
      </group>
    </group>
  );
};

useGLTF.preload('../AVS_MainBuilding.glb');

export default MainBuilding;
