import React, { useRef } from 'react';
import { useGLTF, Edges } from '@react-three/drei';
import * as THREE from 'three';

const Surrounding = (props) => {
  const { nodes, materials } = useGLTF('../newFile.glb');

  return (
    <group {...props} dispose={null}>
      {/* <mesh
        castShadow
        receiveShadow
        geometry={nodes.Plane.geometry}
        material={materials['Material.005']}
        position={[-82.025, 0, 60.399]}
      /> */}
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.Areas.geometry}
        material={materials['Material.001']}
      >
        <Edges scale={1} threshold={15} color="white" />
      </mesh>
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.Wayshighway.geometry}
        material={materials['Material.003']}
      >
        <Edges scale={1} threshold={15} color="white" />
      </mesh>
    </group>
  );
};

useGLTF.preload('../newFile.glb');

export default Surrounding;
