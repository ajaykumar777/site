import { Canvas } from '@react-three/fiber';
import { PerspectiveCamera, OrbitControls, Loader } from '@react-three/drei';
import { Suspense } from 'react';
import Experience from './Experience.jsx';

function App() {
  return (
    <>
      <Canvas>
        <color attach="background" args={['#171263']} />
        <OrbitControls />
        <ambientLight intensity={1.5} />
        <PerspectiveCamera makeDefault position={[3, 2.5, 3]} fov={60} />
        {/* <directionalLight
          position={[1, 2, -1]}
          intensity={1.5}
          color="#ffffff"
        />
        <directionalLight
          position={[1, 2, 1]}
          intensity={2.5}
          color="#ffffff"
        /> */}

        <Suspense fallback={null}>
          <Experience scale={0.01} />
        </Suspense>
      </Canvas>
      <Loader />
    </>
  );
}

export default App;
