import React, { useRef } from 'react';
import { useGLTF, Edges, Environment } from '@react-three/drei';
import Surrounding from './components/Surrounding';
import MainBuilding from './components/MainBuilding';

const Experience = (props) => {
  return (
    <>
      <Environment files="./ballroom_1k.hdr" />
      <group position={[1.9, 0, -1.1]}>
        <Surrounding scale={0.01} />
        <MainBuilding scale={0.01} />
      </group>
    </>
  );
};

export default Experience;
